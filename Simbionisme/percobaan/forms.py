from django import forms
from django.forms import ModelForm
from .models import *
from django.db import connection

class Todo_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    title_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Masukan judul...'
    }
    description_attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 4,
        'class': 'todo-form-textarea',
        'placeholder':'Masukan deskripsi...'
    }

    title = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=title_attrs))
    description = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))

# cursor = connection.cursor()
# list_tabel = cursor.execute("select nama from")

class SB_Form(forms.Form):
    cursor = connection.cursor()
    query = "select kode_skema_beasiswa, kode_skema_beasiswa from skema_beasiswa_aktif where status= '%s' order by kode_skema_beasiswa ASC" % "aktif"
    cursor.execute(query)
    row = cursor.fetchall()

    print(row)
    
    masukanips = 30.00
    masukannpm = '1600957074'
    masukanemail = 'aloper@zmail.com'

    kode_beasiswa = forms.CharField(label='Kode Beasiswa', required=True, max_length=27, widget=forms.Select(choices=row, attrs={'class': 'btn btn-primary dropdown-toggle w-100'}))
    npm = forms.CharField(label='NPM', required=True, widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Enter NPM','id':"npminput","readonly":"true"}), initial=masukannpm)
    email = forms.CharField(label='Email', required=True, widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Enter Email','id':"emailinput","readonly":"true"}), initial=masukanemail)
    ips = forms.CharField(label='Indeks Prestasi Semester', required=True, widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'IPS','id':"npminput","readonly":"true"}), initial=masukanips)

    # class Meta:
    #     nama_attrs = {
    #         'type': 'text',
    #         'class': 'col-sm-12',
    #         'placeholder':'Masukan judul...'
    #     }
    #     description_attrs = {
    #         'type': 'text',
    #         'cols': 50,
    #         'rows': 4,
    #         'class': 'form-input',
    #         'id': 'inlineFormInput',
    #         'placeholder':'Masukan deskripsi...'
    #     }

    #     skema_attrs = {
    #         'class': 'col-sm-12',
    #     }

    #     model = SkemaBeasiswa, 
    #     fields = ['skema', 'judul', 'description']
    #     widgets = {
    #         'name': forms.TextInput(attrs=nama_attrs),
    #         'description': forms.Textarea(attrs=description_attrs),
    #         'skema' : forms.Select(attrs=skema_attrs),
    #     }
