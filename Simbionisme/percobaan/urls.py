from django.conf.urls import url
from .views import *

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^add_todo', add_todo, name='add_todo'),
	url(r'^fpb', fpb, name='fpb'),
	url(r'^lsb', lsb, name='lsb'),
	url(r'^insert_pendaftar', insert_pendaftar, name='insert_pendaftar'),
	url(r'^get_table', get_table, name='get_table'),
	url(r'^delete_todo/(?P<id>\d+)/$', delete_todo, name='delete_todo'),
	url(r'^open_pendaftar/(?P<no_u>\d+)/(?P<kode>\d+)/$', open_pendaftar, name='open_pendaftar'),
	url(r'^terimatolak/(?P<no_u>\d+)/(?P<kode>\d+)/(?P<npm>.+)/(?P<string>.+)/$', terimatolak, name='terimatolak'),

]
