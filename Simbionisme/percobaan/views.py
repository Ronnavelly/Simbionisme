from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import *
from .forms import *
from django.views.generic.base import RedirectView
from django.db import connection
from django.core import serializers
import datetime

# Create your views here.
response = {}
def index(request): 
	cursor = connection.cursor()
	sb = cursor.execute("select * from skema_beasiswa")
	response['sb'] = sb
	sb_form = SB_Form()
	if ('username' in request.session.keys() and request.session['role']=='mahasiswa'):
		username = request.session['username']
		query = "select M.npm, M.email, RA.ips from pengguna P, mahasiswa M, riwayat_akademik RA where P.username='%s' AND P.username=M.username AND M.npm=RA.npm" % username
		sessioncuy = cursor.execute(query)
		anas = cursor.fetchall()[0]
		print(anas)
		sb_form.masukanips = anas[2]
		sb_form.masukannpm = anas[0]
		sb_form.masukanemail = anas[1]

	response['sb_form'] = sb_form
	html = 'percobaan/fpb.html'
	cursor = connection.cursor()
	query = "select kode_skema_beasiswa, no_urut, tgl_tutup_pendaftaran, status, jumlah_pendaftar from skema_beasiswa_aktif"
	cursor.execute(query)
	sba_table = cursor.fetchall()

	return render(request, html, response)

def fpb(request):
	html = 'percobaan/fpb.html'
	return render(request, html, response)

def lsb(request):
	cursor = connection.cursor()
	if ('username' in request.session.keys() and request.session['role']=='donatur'):
		username = request.session['username']
		query = "select SBA.no_urut, SBA.kode_skema_beasiswa, SBA.tgl_tutup_pendaftaran, SBA.status, SBA.jumlah_pendaftar from skema_beasiswa_aktif SBA, skema_beasiswa SB, donatur D where SB.kode=SBA.kode_skema_beasiswa AND D.nomor_identitas=SB.nomor_identitas_donatur AND username = '%s'" % username 
	else:
		query = "select no_urut, kode_skema_beasiswa, tgl_tutup_pendaftaran, status, jumlah_pendaftar from skema_beasiswa_aktif"
	cursor.execute(query)
	tabel_skema = cursor.fetchall()
	response['tabel_skema'] = tabel_skema
	html = 'percobaan/lsb.html'
	return render(request, html, response)

def open_pendaftar(request, no_u, kode):
	cursor = connection.cursor()
	query = "select no_urut, nama, npm, waktu_daftar, status_terima from pendaftaran natural join mahasiswa where no_urut= %s AND kode_skema_beasiswa = %s" % (no_u,kode)
	cursor.execute(query)
	pendaftaran = cursor.fetchall()
	response['tabel_pendaftaran'] = pendaftaran
	response['kode'] = kode
	html = 'percobaan/lpsb.html'
	return render(request, html, response)

def terimatolak(request, no_u, kode, npm, string):

	cursor = connection.cursor()
	query = "update pendaftaran set status_terima='%s' where no_urut='%s' AND kode_skema_beasiswa='%s' AND npm='%s' " % (string, no_u, kode, npm)
	cursor.execute(query)

	cursor = connection.cursor()
	query = "select no_urut, nama, npm, waktu_daftar, status_terima from pendaftaran natural join mahasiswa where no_urut= %s AND kode_skema_beasiswa = %s" % (no_u,kode)
	cursor.execute(query)
	pendaftaran = cursor.fetchall()

	response['tabel_pendaftaran'] = pendaftaran
	html = 'percobaan/lpsb.html'
	return render(request, html, response)

def add_todo(request):
	form = Todo_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['title'] = request.POST['title']
		response['description'] = request.POST['description']
		todo = Todo(title=response['title'],description=response['description'])
		todo.save()
		return HttpResponseRedirect('/percobaan/')
	else:
		return HttpResponseRedirect('/percobaan/')

def delete_todo(request, id):
	sb_delete = sb.objects.get(id=id)
	sb_delete.delete()

	return HttpResponseRedirect('/percobaan/')

def get_table(request):
	cursor = connection.cursor()
	query = "select no_urut ,kode_skema_beasiswa, tgl_tutup_pendaftaran, status, jumlah_pendaftar from skema_beasiswa_aktif"
	cursor.execute(query)
	sba_table = cursor.fetchall()
	data = serializers.serialize('json', sba_table)
	print(data)
	return HttpResponse(data)

def insert_pendaftar(request):
	form = SB_Form(request.POST or None)
	if (request.method == 'POST' and form.is_valid()):
		kode_beasiswa = request.POST['kode_beasiswa']
		NPM = request.POST['npm']
		email = request.POST['email']
		ips = request.POST['ips']
		time = datetime.datetime.now()
		time_real = '{:%Y-%m-%d %H:%M:%S}'.format(time)

		cursor = connection.cursor()
		count_query = "select no_urut from skema_beasiswa_aktif where kode_skema_beasiswa = %s" % kode_beasiswa
		cursor.execute(count_query)
		nobar_pendaftaran = int(cursor.fetchall()[0][0])

		print(nobar_pendaftaran)

		query = "insert into pendaftaran values (%s, %s, '%s', '%s', '%s', '%s')" % (nobar_pendaftaran, kode_beasiswa, NPM, time_real, 'aktif', '-')
		cursor.execute(query)


		cursor = connection.cursor()
		count_query = "select count(*) from pendaftaran"
		cursor.execute(count_query)
		nobar_pendaftaran = int(cursor.fetchall()[0][0])

		print()
		print(nobar_pendaftaran)
		print()
		print()

		return HttpResponseRedirect('/percobaan/')
