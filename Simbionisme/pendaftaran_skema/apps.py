from django.apps import AppConfig


class PendaftaranSkemaConfig(AppConfig):
    name = 'pendaftaran_skema'
