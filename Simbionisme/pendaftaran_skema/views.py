from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages

def daftar_skema(request):
    response={}
    if ('username' in request.session.keys()):
        return render(request, 'pendaftaran_skema.html', response)
    else:
        return HttpResponseRedirect(reverse('account:login'))