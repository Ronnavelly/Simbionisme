from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^login/', indexLogin, name='login'),
    url(r'^submit-login', login, name='submitLogin'),
    url(r'^logout/', logout, name='logout'),
    url(r'^register/', register, name='register'),
    url(r'^submit-mahasiswa', submit_mahasiswa, name='submit-mahasiswa'),
    url(r'^submit-donatur-individu', submit_individual_donor, name='submit-donatur-individual'),
    url(r'^submit-donatur-yayasan', submit_yayasan, name='submit-donatur-yayasan'),
]
